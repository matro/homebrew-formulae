cask 'modd' do
  version '0.4'
  sha256 "6bc460f55a0838ae71bcd47d70c656cdaa6fd2641c232ff0e228aa6bbb4a0c62"

  url "https://github.com/cortesi/modd/releases/download/v#{version}/modd-#{version}-osx64.tgz"
  name 'modd'
  homepage 'https://github.com/cortesi/modd'
  license :mit

  caveats do
    files_in_usr_local
  end

  binary "modd-#{version}-osx64/modd"
  
  uninstall :delete => [
                  '/usr/local/bin/modd'
                 ]
end
