# Matro's Homebrew Formulae #

This is a repository of things to install with Homebrew.

## Install a Thing! ##

 1. Get thee some [Homebrew](http://brew.sh/) (or [Linuxbrew](http://brew.sh/linuxbrew/)) on your system.
 1. `brew tap matro/formulae https://bitbucket.org/matro/homebrew-formulae.git`
 1. `brew install <formula_name>`

There are also some `brew cask` action here.

## Updating ##

Do a `brew update && brew upgrade` and anything installed from this tap should be updated along with everything else that you've homebrewed. Except the Casks. Maybe one day, upgrading Casks will work.

## Help & Info ##

`brew help`, `man brew` or check [Homebrew's documentation](https://github.com/Homebrew/homebrew/tree/master/share/doc/homebrew#readme).

## Development ##

TBD, but here are some docs to guide us as we go to space today:

 * [Create & Maintain a Tap](https://github.com/Homebrew/homebrew/blob/master/share/doc/homebrew/How-to-Create-and-Maintain-a-Tap.md)
 * [Formula Cookbook](https://github.com/Homebrew/homebrew/blob/master/share/doc/homebrew/Formula-Cookbook.md)